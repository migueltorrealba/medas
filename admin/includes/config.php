<?php
session_start();

class Conectar 
{
    public function con()
    {
        $con=mysql_connect("localhost","root","");
        
		mysql_query("SET NAMES 'utf8'");
		mysql_select_db("disgrafica");
		return $con;
    }

    public static function ruta_ingresar()
    {
        return "http://localhost/disgrafica/admin/";
    }
    
    public static function ruta()
    {
        return "http://localhost/disgrafica/admin/";
    }

    public static function ruta_imagen_noticia(){
    	return "public/img/entradas/";
    }

    public static function ruta_imagen_thumb(){
    	return "public/img/entradas/thumb/thumbnail_";
    }

    public static function ruta_imagen_public(){
    	return "../../public/img/banner/";
    }

    public static function ruta_imagen_publica(){
    	return "../public/img/banner/";
    }

    public function comillas_inteligentes($valor)
	{
		self::con();
		// Retirar las barras
		if (get_magic_quotes_gpc()) {
			$valor = stripslashes($valor);
		}
	
		// Colocar comillas si no es entero
		if (!is_numeric($valor)) {
			$valor = "'" . mysql_real_escape_string($valor) . "'";
		}

		return $valor;
	}

	public function cerrar_con()
	{
		$cerrar = mysql_close(self::con());

		return $cerrar;
	}

	 public static function url()
	 {
	     $pageURL = 'http';
	     // if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	     $pageURL .= "://";
	     if ($_SERVER["SERVER_PORT"] != "80") {
	        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	     } else {
	        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	     }
	     return $pageURL;
	 }

	public static function voltear_fecha($fecha)
	{
		$dia = $fecha[8]."".$fecha[9];
		$mes = $fecha[5]."".$fecha[6];
		$anio = $fecha[0]."".$fecha[1]."".$fecha[2]."".$fecha[3];
		$fecha_nueva = $dia."-".$mes."-".$anio;
		
		return $fecha_nueva;
	} 

	public static function voltear_fecha2($fecha)
	{
		$dia = $fecha[0]."".$fecha[1];
		$mes = $fecha[3]."".$fecha[4];
		$anio = $fecha[6]."".$fecha[7]."".$fecha[8]."".$fecha[9];
		$fecha_nueva = $anio."-".$mes."-".$dia;
		
		return $fecha_nueva;
	}

	public function change_status($valor)
	{
		if ($valor == 0) {
			$valor = "Anulado";
		}
		if ($valor == 1) {
			$valor = "Activo";
		}
		return $valor;
	}

	public static function estado_pedido($valor){
		if ($valor == 1) {
			$valor = "Pendiente";
		}elseif ($valor == 2) {
			$valor = "En Proceso";
		}elseif ($valor == 3) {
			$valor = "Anulado";
		}
		return $valor;
	}

	public static function nivel_usuario($valor)
	{
		if ($valor==1) {
			$valor = "Web Master";
		}elseif ($valor==2) {
			$valor = "Administrador";
		}elseif ($valor==3) {
			$valor = "Editor";
		}elseif ($valor==4) {
			$valor = "Cliente";
		}elseif ($valor==5) {
			$valor = "Editor 2";
		}

		return $valor;
	}

	public static function status_usuario($valor)
	{
		if ($valor==0) {
			$valor = "Deshabilitado";
		}elseif ($valor==1) {
			$valor = "Activo";
		}

		return $valor;
	}

	public static function validate_img($type, $img)
	{
		if ($type==2) {
			// Cadena de texto a evaluar
			$nombre_fichero = $img;
			// Sólo se permiten gif, jpg ó jpeg y png
			// sin sensibilidad a letras mayúsculas ni minúsculas
			$patron = "%\.(jpe?g|png)$%i";
			// Ejemplo de visualización del resultado
			if (preg_match($patron, $nombre_fichero) == 1) {
				return True;
			} else{
				return False;
			}
		}
	}

	public static function crearThumbnailRecortado($nombreImagen, $nombreThumbnail, $nuevoAncho, $nuevoAlto){
	    // Obtiene las dimensiones de la imagen.
	    list($ancho, $alto) = getimagesize($nombreImagen);
	        
	    // Si la division del ancho de la imagen entre el ancho del thumbnail es mayor
	    // que el alto de la imagen entre el alto del thumbnail entoces igulamos el 
	    // alto de la imagen  con el alto del thumbnail y calculamos cual deberia ser
	    // el ancho para la imagen (Seria mayor que el ancho del thumbnail). 
	    // Si la relacion entre los altos fuese mayor entonces el altoImagen seria
	    // mayor que el alto del thumbnail.
	    if ($ancho/$nuevoAncho > $alto/$nuevoAlto){
	        $altoImagen = $nuevoAlto;
	        $factorReduccion = $alto / $nuevoAlto;
	        $anchoImagen = $ancho / $factorReduccion;      
	    }
	    else{
	        $anchoImagen = $nuevoAncho;
	        $factorReduccion = $ancho / $nuevoAncho;
	        $altoImagen = $alto / $factorReduccion;
	    }
	     
	    list($imagen, $tipo)= self::abrirImagen($nombreImagen);
	    
	    $thumb = imagecreatetruecolor($nuevoAncho, $nuevoAlto);  
	    if ($ancho/$nuevoAncho > $alto/$nuevoAlto){
	        imagecopyresampled($thumb , $imagen, ($nuevoAncho-$anchoImagen)/2, 0, 0, 0, $anchoImagen, $altoImagen, $ancho, $alto);
	    }  else {
	        imagecopyresampled($thumb , $imagen, 0, ($nuevoAlto-$altoImagen)/2, 0, 0, $anchoImagen, $altoImagen, $ancho, $alto);
	    }
	    
	    self::guardarImagen($thumb, $nombreThumbnail, $tipo);
	}

	public static function guardarImagen($imagen, $nombre, $tipo){
	    switch ($tipo){
	        case "image/jpeg":
	            imagejpeg($imagen, $nombre, 100); // El 100 es la calidade de la imagen (entre 1 y 100. Con 100 sin compresion ni perdida de calidad.).
	            break;
	        case "image/gif":
	            imagegif($imagen, $nombre);
	            break;
	        case "image/png":
	            imagepng($imagen, $nombre, 9); // El 9 es grado de compresion de la imagen (entre 0 y 9. Con 9 maxima compresion pero igual calidad.).
	            break;
	        default :
	            echo "Error: Tipo de imagen no permitido.";
	    }
	}

	public static function abrirImagen($nombre){
	    $info = getimagesize($nombre);
	    switch ($info["mime"]){
	        case "image/jpeg":
	            $imagen = imagecreatefromjpeg($nombre);
	            break;
	        case "image/gif":
	            $imagen = imagecreatefromgif($nombre);
	            break;
	        case "image/png":
	            $imagen = imagecreatefrompng($nombre);
	            break;
	        default :
	            echo "Error: No es un tipo de imagen permitido.";
	    }
	    $resultado[0]= $imagen;
	    $resultado[1]= $info["mime"];
	    return $resultado;
	}


}
?>
