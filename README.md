# README #

Gestor de Contenidos CMS (autogestionable) para los módulos:

a) Catálogo de productos, el administrador o editor del sitio podrá
crear y editar categorías y productos. En nuevos productos, podrá
agregar los siguientes datos: Nombre, Código, Descripción, Fotos,
Video, Especificaciones.

b) Gestor de usuarios. El administrador podrá crear o eliminar los
usuarios del sitio. Existirán distintos niveles de privilegios (Administrador,
Editor, Cliente).


c) Visor de estadísticas. El administrador podrá visualizar la cantidad
de visitar recibidas, la visitas por clientes y los pedidos recibidos por
clientes.


d) Correos automatizados para crear interacción con los clientes
cuando realicen pedidos, llenen el formulario de contacto o registren un
pago. Correos automatizados para alertar sobre un nuevo pedido,
registro de pago o contacto desde el formulario de la web.


e) Registro de pagos. El cliente registrado podrá registrar su pago
(Fecha, monto, número de factura o pedido, Número de Deposito o
Transferencia). El administrador podrá visualizar todos los Pagos
Registrados y recibirá un correo indicando los mismos.


f) Realización de Pedidos. El cliente registrado podrá realizar pedidos
con un mínimo y máximo de piezas. Visualizar los pedidos realizados y
el status de los mismos. El Administrador del sitio podrá visualizar y
cambiar el status de los pedidos.

g) Banner principal tipo slider (muestra los productos destacados o
imágenes promocionales) El Administrador podrá actualizar dicha
imágenes.

h) Gestor de Noticias. El editor o administrador podrá publicar noticias.
Indicar Título, Cuerpo de Texto, foto principal (portada), fotos galería, link
video.